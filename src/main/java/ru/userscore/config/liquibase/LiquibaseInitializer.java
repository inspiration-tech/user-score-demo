package ru.userscore.config.liquibase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

/**
 * Spring-компонент для запуска миграций liquibase при старте приложения
 */
@Component
@Order(Integer.MAX_VALUE)
public class LiquibaseInitializer implements ApplicationRunner {

    private final DataSource dataSource;

    @Value("${spring.jpa.hibernate.ddl-auto}")
    private String ddlAuto;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    public LiquibaseInitializer(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void run(ApplicationArguments args) {
        if (!ddlAuto.equals("none")) {
            return;
        }

        LiquibaseMigrationRunner.builder(dataSource)
                .changelogFilePath("migrations/db.changelog-master.xml")
                .build()
                .runMigration();
    }

}

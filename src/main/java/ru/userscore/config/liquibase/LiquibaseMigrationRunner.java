package ru.userscore.config.liquibase;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Класс для выполнения миграций liquibase
 */
public class LiquibaseMigrationRunner {

    private final static String DEFAULT_CHANGE_LOG_TABLE = "databasechangelog";
    private final static String DEFAULT_CHANGE_LOG_LOCK_TABLE = "databasechangeloglock";
    private final static String DEFAULT_CHANGE_LOG_FILE_PATH = "migrations/db.changelog-master.xml";

    private final DataSource dataSource;
    private final String databaseChangeLogTable;
    private final String databaseChangeLogLockTable;
    private final String changelogFilePath;

    private LiquibaseMigrationRunner(
            DataSource dataSource,
            String databaseChangeLogTable,
            String databaseChangeLogLockTable,
            String changelogFilePath
    ) {
        this.dataSource = dataSource;
        this.databaseChangeLogTable = databaseChangeLogTable;
        this.databaseChangeLogLockTable = databaseChangeLogLockTable;
        this.changelogFilePath = changelogFilePath;
    }

    public void runMigration() {
        try (Connection connection = dataSource.getConnection()) {
            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(
                    new JdbcConnection(connection)
            );
            database.setDatabaseChangeLogTableName(databaseChangeLogTable);
            database.setDatabaseChangeLogLockTableName(databaseChangeLogLockTable);

            try (Liquibase liquibase = new Liquibase(
                    changelogFilePath,
                    new ClassLoaderResourceAccessor(),
                    database
            )) {
                liquibase.update("");
            }

        } catch (SQLException | LiquibaseException e) {
            throw new RuntimeException(e);
        }
    }

    public static Builder builder(DataSource dataSource) {
        return new Builder(dataSource);
    }

    public static class Builder {
        private final DataSource dataSource;
        private String databaseChangeLogTable = DEFAULT_CHANGE_LOG_TABLE;
        private String databaseChangeLogLockTable = DEFAULT_CHANGE_LOG_LOCK_TABLE;
        private String changelogFilePath = DEFAULT_CHANGE_LOG_FILE_PATH;

        public Builder(DataSource dataSource) {
            this.dataSource = dataSource;
        }

        public Builder databaseChangeLogTable(String databaseChangeLogTable) {
            this.databaseChangeLogTable = databaseChangeLogTable;

            return this;
        }

        public Builder databaseChangeLogLockTable(String databaseChangeLogLockTable) {
            this.databaseChangeLogLockTable = databaseChangeLogLockTable;

            return this;
        }

        public Builder changelogFilePath(String changelogFilePath) {
            this.changelogFilePath = changelogFilePath;

            return this;
        }

        public LiquibaseMigrationRunner build() {
            return new LiquibaseMigrationRunner(
                    this.dataSource,
                    this.databaseChangeLogTable,
                    this.databaseChangeLogLockTable,
                    this.changelogFilePath
            );
        }
    }

}

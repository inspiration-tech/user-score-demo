package ru.userscore.services;

import jakarta.persistence.OptimisticLockException;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.userscore.db.repositories.AccountRepository;
import ru.userscore.exceptions.AccountNotFoundException;
import ru.userscore.exceptions.AccountOperationException;

/**
 * Сервис, отвечающий за работу со счетами
 */
@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    /**
     * Начислить баллы на счёт по id
     * @param accountId - id счёта
     * @param amount - количество баллов для пополнения
     * @throws AccountNotFoundException - если счёт не найден
     */
    @Retryable(
        retryFor = { OptimisticLockException.class },
        maxAttempts = 5,
        backoff = @Backoff(delay = 100, maxDelay = 500)
    )
    @Transactional
    public void addScore(@NonNull Long accountId, @NonNull Integer amount) throws AccountNotFoundException {
        var account = accountRepository
                .findById(accountId)
                .orElseThrow(
                    () -> new AccountNotFoundException("Счёт не найден по указанному id")
                );

        account.setScore(account.getScore() + amount);
        accountRepository.save(account);
    }

    /**
     * Списать баллы со счёта по id
     * @param accountId - id счёта
     * @param amount - количество баллов для списания
     * @throws AccountNotFoundException - если счёт не найден
     * @throws AccountOperationException - если ошибка при операции по счёту
     */
    @Retryable(
        retryFor = { OptimisticLockException.class },
        maxAttempts = 5,
        backoff = @Backoff(delay = 100, maxDelay = 500)
    )
    @Transactional
    public void chargeScore(@NonNull Long accountId, @NonNull Integer amount) throws AccountNotFoundException, AccountOperationException {
        var account = accountRepository
                .findById(accountId)
                .orElseThrow(
                        () -> new AccountNotFoundException("Счёт не найден по указанному id")
                );

        int newScore = account.getScore() - amount;

        if (newScore < 0) {
            throw new AccountOperationException("Недостаточно баллов на счёте");
        }

        account.setScore(newScore);
        accountRepository.save(account);
    }

    /**
     * Получить количество баллов на счёте по id
     * @param accountId - id счёта
     * @return количество баллов на счёте
     * @throws AccountNotFoundException - если счёт не найден
     */
    public Integer getBalance(@NonNull Long accountId) throws AccountNotFoundException {
        var account = accountRepository
                .findById(accountId)
                .orElseThrow(
                        () -> new AccountNotFoundException("Счёт не найден по указанному id")
                );

        return account.getScore();
    }

}

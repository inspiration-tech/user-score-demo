package ru.userscore.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.userscore.db.UserRole;
import ru.userscore.db.entities.User;
import ru.userscore.db.repositories.UserRepository;
import ru.userscore.rest.dto.AuthenticationRequest;
import ru.userscore.rest.dto.AuthenticationResponse;
import ru.userscore.rest.dto.RegisterRequest;

/**
 * Сервис, отвечающий за операции, связанные с пользователем
 */
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    /**
     * Зарегистрировать нового пользователя
     * @param request - запрос на регистрацию пользователя
     * @return ответ с токеном
     */
    public AuthenticationResponse register(RegisterRequest request) {
        User user = User.builder()
                .username(request.getUsername())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(UserRole.USER)
                .build();

        userRepository.save(user);

        return AuthenticationResponse.builder()
                .token(jwtService.generateToken(user))
                .build();
    }

    /**
     * Авторизовать пользователя (выдать токен)
     * @param request - запрос на авторизацию пользователя
     * @return ответ с токеном
     */
    public AuthenticationResponse auth(AuthenticationRequest request) {
        authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(
                request.getUsername(),
                request.getPassword()
            )
        );

        User user = userRepository
                .findByUsername(request.getUsername())
                .orElseThrow();

        return AuthenticationResponse.builder()
                .token(jwtService.generateToken(user))
                .build();
    }

}

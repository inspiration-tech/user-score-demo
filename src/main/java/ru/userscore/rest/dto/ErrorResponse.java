package ru.userscore.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Представляет собой http-ответ с ошибкой
 */
@Data
@AllArgsConstructor
public class ErrorResponse {

    private String error;

}

package ru.userscore.rest.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Представляет собой запрос на регистрацию нового пользователя
 */
@Data
@Builder
public class RegisterRequest {

    private String username;
    private String email;
    private String password;

}

package ru.userscore.rest.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Представляет собой запрос на авторизацию
 */
@Data
@Builder
public class AuthenticationRequest {

    private String username;
    private String password;

}

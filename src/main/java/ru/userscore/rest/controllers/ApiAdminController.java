package ru.userscore.rest.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.userscore.exceptions.AccountNotFoundException;
import ru.userscore.exceptions.AccountOperationException;
import ru.userscore.services.AccountService;

@RestController
@RequestMapping(path = "/api/admin", produces = MediaType.APPLICATION_JSON_VALUE)
public class ApiAdminController {

    private final AccountService accountService;

    @Autowired
    public ApiAdminController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/top-up-balance")
    public ResponseEntity<String> topUpBalance(@RequestParam("account_id") Long id, @RequestParam Integer amount) throws AccountNotFoundException {
        accountService.addScore(id, amount);

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode responseNode = objectMapper.createObjectNode();
        responseNode.put("success", true);

        return ResponseEntity.ok(responseNode.toString());
    }

    @PostMapping("/pay")
    public ResponseEntity<String> pay(@RequestParam("account_id") Long id, @RequestParam Integer amount) throws AccountNotFoundException, AccountOperationException {
        accountService.chargeScore(id, amount);

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode responseNode = objectMapper.createObjectNode();
        responseNode.put("success", true);

        return ResponseEntity.ok(responseNode.toString());
    }

}

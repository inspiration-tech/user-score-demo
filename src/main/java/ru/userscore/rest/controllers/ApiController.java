package ru.userscore.rest.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.userscore.exceptions.AccountNotFoundException;
import ru.userscore.services.AccountService;

@RestController
@RequestMapping(path = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class ApiController {

    private final AccountService accountService;

    @Autowired
    public ApiController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/get-balance")
    public ResponseEntity<String> getBalance(@RequestParam("account_id") Long id) throws AccountNotFoundException {
        Integer balance = accountService.getBalance(id);

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode responseNode = objectMapper.createObjectNode();
        responseNode.put("balance", balance);

        return ResponseEntity.ok(responseNode.toString());
    }

}

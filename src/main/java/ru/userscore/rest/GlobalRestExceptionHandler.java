package ru.userscore.rest;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.userscore.exceptions.AccountException;
import ru.userscore.rest.dto.ErrorResponse;

@Slf4j
@RestControllerAdvice(basePackages = {"ru.userscore"})
class GlobalRestExceptionHandler {

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ErrorResponse> handleAccountException(HttpServletRequest req, BadCredentialsException e) {
        String errorText = "Некорректный логин или пароль";

        log.info(e.getClass().getName()+": "+e.getMessage());
        log.debug(e.getMessage(), e);

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(new ErrorResponse(errorText));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<ErrorResponse> handleAccountException(HttpServletRequest req, MissingServletRequestParameterException e) {
        String errorText = "Отсутствует обязательный параметр \""+e.getParameterName()+"\"";

        log.info(e.getClass().getName()+": "+e.getMessage());
        log.debug(e.getMessage(), e);

        return ResponseEntity
                .badRequest()
                .body(new ErrorResponse(errorText));
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorResponse> handleAccountException(HttpServletRequest req, IllegalArgumentException e) {
        String errorText = "Некорректные параметры запроса";

        log.info(e.getClass().getName()+": "+e.getMessage());
        log.debug(e.getMessage(), e);

        return ResponseEntity
                .badRequest()
                .body(new ErrorResponse(errorText));
    }

    @ExceptionHandler(AccountException.class)
    public ResponseEntity<ErrorResponse> handleAccountException(HttpServletRequest req, AccountException e) {
        String errorText = e.getMessage();

        log.info(e.getClass().getName()+": "+errorText);
        log.debug(errorText, e);

        return ResponseEntity
                .status(getExceptionStatus(e))
                .body(new ErrorResponse(errorText));
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorResponse> handleAnyException(HttpServletRequest req, Exception e) {

        String errorText = "На сервере произошла ошибка. Пожалуйста, повторите попытку позднее.";

        log.error(e.getClass().getName()+": "+e.getMessage());
        log.debug(e.getMessage(), e);

        return ResponseEntity
                .status(getExceptionStatus(e))
                .body(new ErrorResponse(errorText));
    }

    private HttpStatus getExceptionStatus(Exception exception) {
        ResponseStatus responseStatus = exception.getClass().getAnnotation(ResponseStatus.class);

        if (responseStatus != null) {
            return responseStatus.value();
        }

        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

}

package ru.userscore.db.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.userscore.db.entities.Account;

public interface AccountRepository extends CrudRepository<Account, Long> {
}

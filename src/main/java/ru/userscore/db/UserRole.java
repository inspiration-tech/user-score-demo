package ru.userscore.db;

/**
 * Роль пользователя в системе
 */
public enum UserRole {
    USER,
    ADMIN
}

package ru.userscore.db.entities;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

/**
 * Сущность, представляющая собой счёт пользователя
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(
    name = "accounts",
    indexes = {
        @Index(columnList = "user_id"),
    }
)
public class Account extends AbstractEntity {

    @ManyToOne
    protected User user;

    /**
     * Баллы на счету
     */
    @Column(name = "score", nullable = false)
    protected Integer score = 0;

    /**
     * Версия для optimistic-лока
     */
    @Version
    @Column(name = "version", nullable = false)
    protected Long version = 0L;

}

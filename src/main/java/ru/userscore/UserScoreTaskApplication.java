package ru.userscore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserScoreTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserScoreTaskApplication.class, args);
    }

}

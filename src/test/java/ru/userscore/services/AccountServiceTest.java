package ru.userscore.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.userscore.db.entities.Account;
import ru.userscore.db.entities.User;
import ru.userscore.db.repositories.AccountRepository;
import ru.userscore.exceptions.AccountNotFoundException;
import ru.userscore.exceptions.AccountOperationException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class AccountServiceTest {

    @Mock
    private User userMock;

    @Mock
    private AccountRepository accountRepositoryMock;

    @InjectMocks
    private AccountService accountService;

    private AutoCloseable openMocks;

    @BeforeEach
    public void setUp() {
        openMocks = openMocks(this);
        when(userMock.getId()).thenReturn(1L);
    }

    @AfterEach
    void closeService() throws Exception {
        openMocks.close();
    }

    @Test
    void testAddScore_Success() throws AccountNotFoundException {
        Long accountId = 1L;
        Integer initialScore = 100;
        Integer amountToAdd = 50;
        Account account = Account.builder()
                .id(accountId)
                .score(initialScore)
                .build();

        when(accountRepositoryMock.findById(accountId)).thenReturn(Optional.of(account));

        accountService.addScore(accountId, amountToAdd);

        assertEquals(initialScore + amountToAdd, account.getScore());
        verify(accountRepositoryMock, times(1)).findById(accountId);
        verify(accountRepositoryMock, times(1)).save(account);
    }

    @Test
    void testAddScore_AccountNotFound() {
        Long accountId = 1L;
        Integer amountToAdd = 50;
        when(accountRepositoryMock.findById(accountId)).thenReturn(Optional.empty());

        assertThrows(AccountNotFoundException.class, () -> accountService.addScore(accountId, amountToAdd));
        verify(accountRepositoryMock, times(1)).findById(accountId);
        verify(accountRepositoryMock, never()).save(any());
    }

    @Test
    void testChargeScore_Success() throws AccountNotFoundException, AccountOperationException {
        Long accountId = 1L;
        Integer initialScore = 100;
        Integer amountToCharge = 50;
        Account account = Account.builder()
                .id(accountId)
                .score(initialScore)
                .build();

        when(accountRepositoryMock.findById(accountId)).thenReturn(Optional.of(account));

        accountService.chargeScore(accountId, amountToCharge);

        assertEquals(initialScore - amountToCharge, account.getScore());
        verify(accountRepositoryMock, times(1)).findById(accountId);
        verify(accountRepositoryMock, times(1)).save(account);
    }

    @Test
    void testChargeScore_AccountNotFound() {
        Long accountId = 1L;
        Integer amountToCharge = 50;
        when(accountRepositoryMock.findById(accountId)).thenReturn(Optional.empty());

        assertThrows(AccountNotFoundException.class, () -> accountService.chargeScore(accountId, amountToCharge));
        verify(accountRepositoryMock, times(1)).findById(accountId);
        verify(accountRepositoryMock, never()).save(any());
    }

    @Test
    void testChargeScore_InsufficientBalance() {
        Long accountId = 1L;
        Integer initialScore = 50;
        Integer amountToCharge = 100;
        Account account = Account.builder()
                .id(accountId)
                .score(initialScore)
                .build();

        when(accountRepositoryMock.findById(accountId)).thenReturn(Optional.of(account));

        assertThrows(AccountOperationException.class, () -> accountService.chargeScore(accountId, amountToCharge));
        assertEquals(initialScore, account.getScore());
        verify(accountRepositoryMock, times(1)).findById(accountId);
        verify(accountRepositoryMock, never()).save(any());
    }

    @Test
    void testGetBalance_Success() throws AccountNotFoundException {
        Long accountId = 1L;
        Integer expectedScore = 100;
        Account account = Account.builder()
                .id(accountId)
                .score(expectedScore)
                .build();

        when(accountRepositoryMock.findById(accountId)).thenReturn(Optional.of(account));

        Integer balance = accountService.getBalance(accountId);

        assertEquals(expectedScore, balance);
        verify(accountRepositoryMock, times(1)).findById(accountId);
    }

    @Test
    void testGetBalance_AccountNotFound() {
        Long accountId = 1L;
        when(accountRepositoryMock.findById(accountId)).thenReturn(Optional.empty());

        assertThrows(AccountNotFoundException.class, () -> accountService.getBalance(accountId));
        verify(accountRepositoryMock, times(1)).findById(accountId);
    }

}

package ru.userscore.services;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.util.ReflectionTestUtils;
import ru.userscore.db.UserRole;

import java.math.BigDecimal;
import java.security.Key;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class JwtServiceTest {

    private final static String SECRET_KEY = "4CDB165C8F26669FF5516EE3047238945B99E372A7FCF97EC17A915C3BACA499DD40C55883C7F567B23EE1D808181649841E1B637B0BDFB6928EDA36A749FCE39B827865586F2BB6C0C5F49971B7F57205C61F869FF00F0E6015B0CD0C57FBF277944F97A4C42D92074BB20560DA99A4E264845BB3C8203DADC10BA507B71AC4";
    private final static String TEST_USERNAME = "test_user";
    private final static String TEST_PASSWORD = "1234567";

    @InjectMocks
    private JwtService jwtServiceUnderTest;

    private AutoCloseable openMocks;

    @BeforeEach
    public void setUp() {
        openMocks = openMocks(this);
        ReflectionTestUtils.setField(jwtServiceUnderTest, "secretKey", SECRET_KEY);
    }

    @AfterEach
    void closeService() throws Exception {
        openMocks.close();
    }

    @Test
    void testExtractUsername() {
        String testUsername = TEST_USERNAME;
        String jwt = createJwt(testUsername, new Date(), new Date(System.currentTimeMillis() + 3600 * 1000));
        String extractedUsername = jwtServiceUnderTest.extractUsername(jwt);

        assertEquals(testUsername, extractedUsername);
    }

    @Test
    void testExtractExpiration() {
        Date testExpiration = new Date(System.currentTimeMillis() + 3600 * 1000);
        String jwt = createJwt(TEST_USERNAME, new Date(), testExpiration);
        Date extractedExpiration = jwtServiceUnderTest.extractExpiration(jwt);

        assertEquals(testExpiration.toString(), extractedExpiration.toString());
    }

    @Test
    void testExtractClaim() {
        UserDetails userDetails = new User(TEST_USERNAME, TEST_PASSWORD, Collections.emptyList());
        String additionalClaimKey = "additional_data";
        Map<String, Object> extraClaims = new HashMap<>();
        extraClaims.put(additionalClaimKey, BigDecimal.TEN);

        String generatedToken = jwtServiceUnderTest.generateToken(userDetails, extraClaims);
        BigDecimal bigDecimalClaim = jwtServiceUnderTest.extractClaim(
                generatedToken,
                (claims) -> BigDecimal.valueOf(
                        (Integer) claims.get(additionalClaimKey)
                )
        );

        assertEquals(BigDecimal.TEN, bigDecimalClaim);
    }

    @Test
    void testGenerateToken() {
        UserDetails userDetails = new User(TEST_USERNAME, TEST_PASSWORD, Collections.emptyList());
        Map<String, Object> extraClaims = new HashMap<>();
        extraClaims.put("additional_data", 1000);

        String generatedToken = jwtServiceUnderTest.generateToken(userDetails, extraClaims);

        assertNotNull(generatedToken);
        assertTrue(generatedToken.matches("[a-zA-Z0-9-_]+\\.[a-zA-Z0-9-_]+\\.[a-zA-Z0-9-_]+"));
    }

    @Test
    void testIsTokenValid() {
        UserDetails userDetails = new User(TEST_USERNAME, TEST_PASSWORD, List.of(new SimpleGrantedAuthority(UserRole.USER.name())));
        String jwt = createJwt(TEST_USERNAME, new Date(), new Date(System.currentTimeMillis() + 3600 * 1000));

        boolean isValid = jwtServiceUnderTest.isTokenValid(jwt, userDetails);

        assertTrue(isValid);
    }

    @Test
    void testIsTokenNotValid() {
        UserDetails userDetails = new User("wrong_username", TEST_PASSWORD, List.of(new SimpleGrantedAuthority(UserRole.USER.name())));
        String jwt = createJwt(TEST_USERNAME, new Date(), new Date(System.currentTimeMillis() + 3600 * 1000));

        boolean isValid = jwtServiceUnderTest.isTokenValid(jwt, userDetails);

        assertFalse(isValid);
    }


    @Test
    void testIsTokenExpired() {
        UserDetails userDetails = new User(TEST_USERNAME, TEST_PASSWORD, List.of(new SimpleGrantedAuthority(UserRole.USER.name())));
        String jwt = createJwt(TEST_USERNAME, new Date(), new Date(System.currentTimeMillis() - 1));

        boolean isValid = jwtServiceUnderTest.isTokenValid(jwt, userDetails);

        assertFalse(isValid);
    }

    private String createJwt(String subject, Date issuedAt, Date expiration) {
        Key key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(SECRET_KEY));
        JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(subject)
                .setIssuedAt(issuedAt)
                .setExpiration(expiration)
                .signWith(key);

        return jwtBuilder.compact();
    }

}

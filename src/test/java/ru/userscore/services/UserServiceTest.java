package ru.userscore.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.userscore.db.UserRole;
import ru.userscore.db.entities.User;
import ru.userscore.db.repositories.UserRepository;
import ru.userscore.rest.dto.AuthenticationRequest;
import ru.userscore.rest.dto.AuthenticationResponse;
import ru.userscore.rest.dto.RegisterRequest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private JwtService jwtService;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserService userService;

    private AutoCloseable openMocks;

    @BeforeEach
    public void setUp() {
        openMocks = openMocks(this);
    }

    @AfterEach
    void closeService() throws Exception {
        openMocks.close();
    }

    /**
     * Успешная регистрация
     */
    @Test
    void testRegisterSuccess() {
        String testEncryptedPassword = "encodedPassword";
        String testGeneratedToken = "generatedToken";

        RegisterRequest registerRequest = RegisterRequest.builder()
                .username("new_user")
                .email("user@example.com")
                .password("1234567")
                .build();

        when(passwordEncoder.encode(registerRequest.getPassword())).thenReturn(testEncryptedPassword);
        when(jwtService.generateToken(any(User.class))).thenReturn(testGeneratedToken);

        AuthenticationResponse response = userService.register(registerRequest);

        assertEquals(testGeneratedToken, response.getToken());
        verify(passwordEncoder).encode(registerRequest.getPassword());
        verify(userRepository).save(any(User.class));
        verify(jwtService).generateToken(any(User.class));
    }

    /**
     * Успешная авторизация
     */
    @Test
    void testAuthSuccess() {
        String testGeneratedToken = "generatedToken";

        AuthenticationRequest authenticationRequest = AuthenticationRequest.builder()
                .username("new_user")
                .password("1234567")
                .build();

        User user = User.builder()
                .username(authenticationRequest.getUsername())
                .email("user@example.com")
                .password("encodedPassword")
                .role(UserRole.USER)
                .build();

        when(userRepository.findByUsername(authenticationRequest.getUsername())).thenReturn(Optional.of(user));
        when(jwtService.generateToken(user)).thenReturn(testGeneratedToken);

        AuthenticationResponse response = userService.auth(authenticationRequest);

        assertEquals(testGeneratedToken, response.getToken());
        verify(authenticationManager).authenticate(any(UsernamePasswordAuthenticationToken.class));
        verify(userRepository).findByUsername(authenticationRequest.getUsername());
        verify(jwtService).generateToken(user);
    }

    /**
     * Неуспешная авторизация
     */
    @Test
    void testAuthBadCredentials() {
        AuthenticationRequest authenticationRequest = AuthenticationRequest.builder()
                .username("new_user")
                .password("1234567")
                .build();

        when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class)))
                .thenThrow(new BadCredentialsException("Неверный логин или пароль"));

        assertThrows(BadCredentialsException.class, () -> userService.auth(authenticationRequest));

        verify(authenticationManager).authenticate(
                new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
        );
        verifyNoInteractions(userRepository);
        verifyNoInteractions(jwtService);
    }

}

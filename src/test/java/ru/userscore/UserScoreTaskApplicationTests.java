package ru.userscore;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.userscore.db.UserRole;
import ru.userscore.db.entities.Account;
import ru.userscore.db.entities.User;
import ru.userscore.db.repositories.AccountRepository;
import ru.userscore.db.repositories.UserRepository;
import ru.userscore.rest.dto.AuthenticationRequest;
import ru.userscore.rest.dto.AuthenticationResponse;
import ru.userscore.rest.dto.RegisterRequest;
import ru.userscore.services.JwtService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Testcontainers
@AutoConfigureMockMvc
@Transactional
@SpringBootTest(properties = {
    "spring.config.location=classpath:test.properties"
})
public class UserScoreTaskApplicationTests {

    @Container
    private static final PostgreSQLContainer<?> postgresContainer =
            new PostgreSQLContainer<>("postgres:16.2-alpine3.19")
                    .withDatabaseName("testdb")
                    .withUsername("test")
                    .withPassword("test");

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private MockMvc mockMvc;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * Авторизация с неверным логином/паролем
     */
    @Test
    public void testLoginBadCredentials() throws Exception {
        createTestAdmin();

        ObjectMapper objectMapper = new ObjectMapper();
        AuthenticationRequest authRequest = AuthenticationRequest.builder()
                .username("admin")
                .password("12345") // неверный пароль
                .build();
        final String requestJson = objectMapper.writeValueAsString(authRequest);

        mockMvc.perform(
                MockMvcRequestBuilders
                    .post("/auth/authenticate")
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .content(requestJson)
                )
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    /**
     * Успешная авторизация админа с возможностью последующего пополнения баланса
     */
    @Test
    public void testLoginSuccess() throws Exception {
        createTestAdmin();
        createTestUserWithAccounts(0, 300);

        ObjectMapper objectMapper = new ObjectMapper();
        AuthenticationRequest authRequest = AuthenticationRequest.builder()
                .username("admin")
                .password("123456") // верный пароль
                .build();
        final String requestJson = objectMapper.writeValueAsString(authRequest);

        MockHttpServletResponse response = mockMvc.perform(
                    MockMvcRequestBuilders
                        .post("/auth/authenticate")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content(requestJson)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse();

        String responseContent = response.getContentAsString();
        AuthenticationResponse authResponse = objectMapper.readValue(responseContent, AuthenticationResponse.class);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>() {{
            put("account_id", List.of("2"));
            put("amount", List.of("150"));
        }};

        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/api/admin/top-up-balance")
                                .header(HttpHeaders.AUTHORIZATION, "Bearer " + authResponse.getToken())
                                .params(params)
                )
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Успешная регистрация с возможностью последующего запроса баланса
     */
    @Test
    public void testRegistrationSuccess() throws Exception {
        createTestUserWithAccounts(1000, 0);

        ObjectMapper objectMapper = new ObjectMapper();
        RegisterRequest authRequest = RegisterRequest.builder()
                .username("new_user")
                .email("new_user@example.com")
                .password("123456")
                .build();
        final String requestJson = objectMapper.writeValueAsString(authRequest);

        MockHttpServletResponse response = mockMvc.perform(
                    MockMvcRequestBuilders
                        .post("/auth/register")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content(requestJson)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse();

        String responseContent = response.getContentAsString();
        AuthenticationResponse authResponse = objectMapper.readValue(responseContent, AuthenticationResponse.class);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>() {{
            put("account_id", List.of("1"));
        }};

        ObjectNode responseNode = objectMapper.createObjectNode();
        responseNode.put("balance", 1000);

        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/api/get-balance")
                                .header(HttpHeaders.AUTHORIZATION, "Bearer " + authResponse.getToken())
                                .params(params)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(responseNode.toString()));

        User foundUser = userRepository.findByUsername("new_user").orElseThrow();

        assertEquals("new_user@example.com", foundUser.getEmail());
    }

    /**
     * Доступ к запросу баланса для неавторизованного пользователя должен быть запрещён
     */
    @Test
    public void testAccessGetBalanceForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/get-balance"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Запрос баланса без параметров должен приводить к статусу bad request
     */
    @Test
    @WithMockUser(username = "regularUser")
    public void testAccessGetBalanceNoParam() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/get-balance"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    /**
     * Запрос баланса по несуществующему аккаунту должен приводить к статусу not found
     */
    @Test
    @WithMockUser(username = "regularUser")
    public void testAccessGetBalanceNotFound() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>() {{
            put("account_id", List.of("100"));
        }};

        mockMvc.perform(
                    MockMvcRequestBuilders
                        .post("/api/get-balance")
                        .params(params)
                )
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    /**
     * Успешный запрос баланса, обычный пользователь
     */
    @Test
    @WithMockUser(username = "regularUser")
    public void testGetBalanceSuccess() throws Exception {
        createTestUserWithAccounts(0, 300);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>() {{
            put("account_id", List.of("2"));
        }};

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode responseNode = objectMapper.createObjectNode();
        responseNode.put("balance", 300);

        mockMvc.perform(
                    MockMvcRequestBuilders
                        .post("/api/get-balance")
                        .params(params)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(responseNode.toString()));
    }

    /**
     * Успешный запрос баланса, админ
     */
    @Test
    @WithMockUser(username = "admin", roles = {"ADMIN"})
    public void testGetBalanceAdminSuccess() throws Exception {
        createTestUserWithAccounts(0, 300);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>() {{
            put("account_id", List.of("2"));
        }};

        mockMvc.perform(
                    MockMvcRequestBuilders
                        .post("/api/get-balance")
                        .params(params)
                )
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Доступ к пополнению баланса для неавторизованного пользователя должен быть запрещён
     */
    @Test
    public void testAccessTopUpBalanceForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/admin/top-up-balance"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Обычный пользователь не должен иметь доступа к пополнению баланса
     */
    @Test
    @WithMockUser(username = "regularUser")
    public void testAccessTopUpBalanceForbidden2() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/admin/top-up-balance"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Тест успешного пополнения баланса админом
     */
    @Test
    public void testAccessTopUpBalanceAdminSuccess() throws Exception {
        createTestAdmin();
        createTestUserWithAccounts(0, 300);

        UserDetails userDetails = userDetailsService.loadUserByUsername("admin");
        String jwtToken = jwtService.generateToken(userDetails);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>() {{
            put("account_id", List.of("2"));
            put("amount", List.of("150"));
        }};

        mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/api/admin/top-up-balance")
                        .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken)
                        .params(params)
                )
                .andExpect(MockMvcResultMatchers.status().isOk());

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode responseNode = objectMapper.createObjectNode();
        responseNode.put("balance", 450);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/api/get-balance")
                        .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken)
                        .params(params)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(responseNode.toString()));
    }

    /**
     * Доступ к списанию с баланса для неавторизованного пользователя должен быть запрещён
     */
    @Test
    public void testAccessPayForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/admin/pay"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Обычный пользователь не должен иметь доступа к списанию баллов с баланса
     */
    @Test
    @WithMockUser(username = "regularUser")
    public void testAccessPayForbidden2() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/admin/pay"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Тест успешного списания баллов с баланса админом
     */
    @Test
    public void testAccessPayAdminSuccess() throws Exception {
        createTestAdmin();
        createTestUserWithAccounts(100, 750);

        UserDetails userDetails = userDetailsService.loadUserByUsername("admin");
        String jwtToken = jwtService.generateToken(userDetails);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>() {{
            put("account_id", List.of("2"));
            put("amount", List.of("150"));
        }};

        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/api/admin/pay")
                                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken)
                                .params(params)
                )
                .andExpect(MockMvcResultMatchers.status().isOk());

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode responseNode = objectMapper.createObjectNode();
        responseNode.put("balance", 600);

        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/api/get-balance")
                                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken)
                                .params(params)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(responseNode.toString()));
    }

    /**
     * Тест репозиториев
     */
    @Test
    public void testRepositories() {
        User user = new User();
        user.setUsername("new_user");
        user.setEmail("new_user@example.com");
        user.setPassword("123456");
        user.setRole(UserRole.USER);

        userRepository.save(user);

        Account account = Account.builder()
                .user(user)
                .score(1100)
                .build();

        accountRepository.save(account);

        User foundUser = userRepository.findByUsername("new_user").orElseThrow();
        Account foundAccount = accountRepository.findById(1L).orElseThrow();

        assertEquals("new_user@example.com", foundUser.getEmail());
        assertEquals(1100, foundAccount.getScore());
    }

    private void createTestUserWithAccounts(int account1score, int account2score) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String nowDateString = dateFormat.format(new Date());

        // BCrypt пароль 123456
        jdbcTemplate.execute("INSERT INTO users (id, username, email, password, role, created_at) VALUES (1, 'regularUser', 'regular_user@example.com', '$2a$10$fBCwuPsz5vHsun9MF1ICv.xWNZwqHfRElZ4axGldjHQjkiPQoFavy', 'USER', '" + nowDateString + "')");

        jdbcTemplate.execute("INSERT INTO accounts (id, user_id, score, version, created_at) VALUES (1, 1, " + account1score + ", 0, '" + nowDateString + "')");

        jdbcTemplate.execute("INSERT INTO accounts (id, user_id, score, version, created_at) VALUES (2, 1, " + account2score + ", 0, '" + nowDateString + "')");
    }

    private void createTestAdmin() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String nowDateString = dateFormat.format(new Date());

        // BCrypt пароль 123456
        jdbcTemplate.execute("INSERT INTO users (id, username, email, password, role, created_at) VALUES (2, 'admin', 'admin@example.com', '$2a$10$fBCwuPsz5vHsun9MF1ICv.xWNZwqHfRElZ4axGldjHQjkiPQoFavy', 'ADMIN', '" + nowDateString + "')");
    }

}

#!/bin/bash

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

docker compose --file $script_dir/docker-compose_postgres.yml \
               --env-file $script_dir/postgres.env \
               --project-name user-score-test \
               down

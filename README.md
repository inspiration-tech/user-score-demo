ТЕСТОВЫЙ ПРОЕКТ
===============

## Запуск контейнера PostgreSQL

В корне проекта находятся следующие файлы:

- [docker-compose_postgres.yml](docker-compose_postgres.yml) - для запуска ***postgres*** через ***docker compose***
- [postgres.env](postgres.env) - переменные окружения для ***postgres***
- [up_pg.sh](up_pg.sh) - баш-скрипт для запуска postgres-контейнера (чтобы не писать команду полностью)
- [down_pg.sh](down_pg.sh) - баш-скрипт для остановки и удаления postgres-контейнера (чтобы не писать команду полностью)

Соответственно, для запуска postgres-контейнера необходимо выполнить:

```shell
./up_pg.sh
```

(по умолчанию будет использован порт _5432_, поэтому если он занят, необходимо указать другой порт в [postgres.env](postgres.env))

для остановки и удаления postgres-контейнера:

```shell
./down_pg.sh
```

В системе должен быть установлен ***bash***.

## Запуск Spring-приложения

1. Предполагается наличие на локалхосте запущенного postgres-сервера версии _16.2_. Для упрощения предоставлен compose-файл [docker-compose_postgres.yml](docker-compose_postgres.yml). Имя пользователя, пароль, название БД, и другие параметры указаны в файле [postgres.env](postgres.env).
2. Предполагается наличие ***jdk*** версии _17_.
3. Приложение необходимо запускать со следующими переменными окружения (значения из файла [postgres.env](postgres.env)):
- POSTGRES_HOST
- POSTGRES_PORT
- POSTGRES_DB
- POSTGRES_USER
- POSTGRES_PASSWORD
3. Для запуска через ***IntelliJ*** можно использовать Run Configuration [UserScoreTaskApplication](.run/UserScoreTaskApplication.run.xml) (в данной конфигурации используется `temurin-17` в качестве ***jdk***, и `PostgreSQL`) или [InMemoryDatabaseApp](.run/InMemoryDatabaseApp.run.xml) (то же самое, но вместо `PostgreSQL` используется `H2`)
4. В приложении пароли для пользователей, созданных по умолчанию: _123456_ (только если выполнена миграция, т.е. при использовании ***PostgreSQL*** - где _spring.jpa.hibernate.ddl-auto_ = none)
5. Миграция ***liquibase*** создаёт две таблицы и заполняет тестовыми данными при старте приложения (см. файл [LiquibaseInitializer](src/main/java/ru/userscore/config/liquibase/LiquibaseInitializer.java))

## Краткое описание механики

1. Всем пользователям открыта возможность регистрации и авторизации, остальные возможности закрыты для неавторизованных пользователей
2. Все авторизованные пользователи могут запрашивать баллы на балансе по id аккаунта
3. Только админы (роль ***ADMIN***) могут начислять или списывать баллы по id аккаунта

## Описание тестов

- 3 unit-теста на три основных сервиса ([AccountService](src/main/java/ru/userscore/services/AccountService.java), [JwtService](src/main/java/ru/userscore/services/JwtService.java), [UserService](src/main/java/ru/userscore/services/UserService.java))
- 1 интеграционный тест с использованием `testcontainers`
- для ручного тестирования запросов - файл [requests.http](requests.http)
- есть Run Configuration для ***Intellij***: [All Tests](.run/All Tests.run.xml)
